# AstrelSpace

A portfolio/game site made with [Two.js](https://two.js.org), [Gatsby](https://gatsbyjs.com), and TypeScript.

View here: [https://astrelion.gitlab.io/astrel-space/space](https://astrelion.gitlab.io/astrel-space/space)

## Develop

```shell
npm run develop
```

View dev site at [localhost:8000/space](localhost:8000/space).