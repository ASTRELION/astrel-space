import Two from "two.js";
import { Anchor } from "two.js/src/anchor";
import { Group } from "two.js/src/group";
import { Path } from "two.js/src/path";
import { Line } from "two.js/src/shapes/line";
import { Polygon } from "two.js/src/shapes/polygon";
import { Vector } from "two.js/src/vector";
import * as Controls from "./controls"
import { GameObject } from "./gameObject";
import { Projectile } from "./projectile";
import { RigidBody } from "./rigidbody";
import { Space } from "./space";

export class Ship extends RigidBody
{
    constructor(space: Space)
    {
        super(space);
        this.space = space;
        this.two = space.two;
        this.direction = new Vector(0, -1);
        this.velocity = 0;
        this.maxVelocity = 0.3;
        this.drag = 0.0025;
        this.mass = 100;
        this.collision = true;
    }

    make()
    {
        let shape = new Path([
            new Anchor(0, -12),
            new Anchor(12, 12),
            new Anchor(0, 10),
            new Anchor(-12, 12),
            new Anchor(0, -12),
        ], true);
        shape.stroke = "#aaaaaa";
        shape.linewidth = 4;

        this.group = new Group(shape);
        this.group.position.set(this.two.width / 2, this.two.height / 2);
        this.space.add(this);
    }

    rotate(radians: number)
    {
        this.group.rotation += radians;
        let rot = this.group.rotation - (Math.PI / 2);
        this.direction.set(Math.cos(rot), Math.sin(rot));
    }

    update(delta: number)
    {
        if (Controls.actions.UP.down)
        {
            this.addVelocity(0.01);
        }
        else if (Controls.actions.DOWN.down)
        {
            this.subVelocity(0.01);
        }
        else
        {
            this.applyDrag(delta);
        }
        
        if (Controls.actions.LEFT.down)
        {
            this.rotate(-0.0075 * delta);
        }
        else if (Controls.actions.RIGHT.down)
        {
            this.rotate(0.0075 * delta);
        }

        if (Controls.actions.SHOOT.down)
        {
            let bullet1 = new Projectile(this.space);
            let line1 = new Line(
                this.group.position.x, 
                this.group.position.y, 
                this.group.position.x + this.direction.x * 10, 
                this.group.position.y + this.direction.y * 10
            );
            line1.linewidth = 2;
            line1.fill = "#ffffff";
            line1.stroke = "#ffffff";
            bullet1.group = new Group(line1);
            bullet1.direction = this.direction.clone();
            bullet1.velocity = 1;
            bullet1.drag = 0;
            bullet1.startPosition = bullet1.group.position.clone();
            let dir1 = bullet1.direction.clone().rotate(Math.PI / 2).multiply(-5);
            bullet1.group.position.set(dir1.x, dir1.y);

            let bullet2 = bullet1.clone();
            let dir2 = bullet2.direction.clone().rotate(Math.PI / 2).multiply(5);
            bullet2.group.position.set(dir2.x, dir2.y);

            this.space.add(bullet1);
            this.space.add(bullet2);
        }

        // apply drag
        //this.applyDrag(delta);
        this.applyVelocity(delta);
    }
}