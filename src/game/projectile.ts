import { Vector } from "two.js/src/vector";
import { RigidBody } from "./rigidbody";
import { Space } from "./space";

export class Projectile extends RigidBody
{
    startPosition: Vector;
    lifetime: number;

    constructor(space: Space)
    {
        super(space);
        this.startPosition = new Vector(0, 0);
        this.lifetime = 1000;
    }

    update(delta: number)
    {
        super.update(delta);

        if (this.group.position.distanceTo(this.startPosition) >= this.lifetime)
        {
            this.space.remove(this);
        }
    }

    refresh()
    {
        this.startPosition.set(this.group.position.x, this.group.position.y);
    }

    clone()
    {
        let rb = new Projectile(this.space);
        Object.assign(rb, super.clone());
        return rb;
    }
}