import React, { useEffect, useRef } from "react";
import Two from "two.js";
import { Circle } from "two.js/src/shapes/circle";
import { Text } from "two.js/src/text";
import { Group } from "two.js/src/group";
import { Ship } from "./ship";
import * as Controls from "./controls"
import { Asteroid } from "./asteroid";
import { ZUI } from "two.js/extras/jsm/zui";
import { GameObject } from "./gameObject";
import { RigidBody } from "./rigidbody";
import { Rectangle } from "two.js/src/shapes/rectangle";
import { RoundedRectangle } from "two.js/src/shapes/rounded-rectangle";
import { makeWASD } from "./ui/hints";
import { Color } from "./util";

export class Space
{
    two: Two;
    ui: Group;
    camera: Group;
    
    fpsText: Text;
    stars: Circle[] = [];
    ship: Ship;
    oldTime: number = 0.0;
    fpsTime: number = 0.0;
    mouseText: Text;
    frames: number = 0;

    objects: GameObject[] = [];

    constructor()
    {
        this.two = new Two({
            fullscreen: true
        });

        this.two.renderer.domElement.style.background = "#101010";

        this.ui = this.two.makeGroup();

        let ui = new Group();
        let scene = new Group();
        let camera = new ZUI(scene);

        // Stars
        for (let i = 0; i < 100; i++)
        {
            let star = this.two.makeCircle(
                Math.random() * this.two.width,
                Math.random() * this.two.height,
                Math.round(Math.random()) + 1
            );
            //this.camera.add(star);
            this.stars.push(star);
        }

        // FPS text
        this.fpsText = this.two.makeText("0", 25, 25);
        this.fpsText.fill = Color.white;

        this.mouseText = this.two.makeText("0, 0", 50, this.two.height - 25);
        this.mouseText.fill = Color.white;

        let urlText = this.two.makeText("https://gitlab.com/ASTRELION/astrel-space", this.two.width / 2, this.two.height - 25);
        urlText.fill = Color.white;

        this.camera = this.two.makeGroup();

        // Ship
        this.ship = new Ship(this);
        this.ship.make();

        this.objects.push(makeWASD(this));
        
        this.two.add(scene, ui);
    }

    start(element: HTMLElement)
    {
        this.two.appendTo(element);
        window.requestAnimationFrame((t) => this.update(t));
    }

    update(time: number)
    {
        window.requestAnimationFrame((t) => this.update(t));

        // time in milliseconds between renders
        let delta = time - this.oldTime;
        this.oldTime = time;

        // ignore same-frame renders
        if (Math.floor(delta) <= 0)
        {
            return;
        }

        // update FPS every ~second
        if (time >= this.fpsTime + 1000)
        {
            this.setFPS(time - this.fpsTime);
            this.fpsTime = time;
            this.frames = 0;
        }

        this.mouseText.value = `${Controls.mousePosition.x}, ${Controls.mousePosition.y}`;

        for (let obj of this.objects)
        {
            if (obj instanceof RigidBody)
            {
                for (let other of this.objects)
                {
                    if (other != obj && other instanceof RigidBody && obj.collidesWith(other))
                    {
                        obj.collide(other, delta);
                    }
                }
            }

            obj.update(delta);
        }

        this.two.render();
        this.frames++;
    }

    setFPS(delta: number)
    {
        let fps = this.frames / (1000.0 / delta);

        if (fps < Infinity)
        {
            this.fpsText.value = Math.round(fps).toString();
        }
    }

    add(rigidbody: RigidBody)
    {
        this.two.add(rigidbody.group);
        this.objects.push(rigidbody);
    }

    remove(rigidbody: RigidBody)
    {
        this.two.remove(rigidbody.group);
        let i = this.objects.indexOf(rigidbody);
        if (i >= 0)
        {
            this.objects.splice(i, 1);
        }
    }
}

export default function App() 
{
    let domElement = useRef();
  
    useEffect(setup, []);
  
    function setup() 
    {
        Controls.init(domElement.current);
        let space = new Space();
        space.start(domElement.current);
    
        return unmount;
    
        function unmount()
        {
            space.two.pause();
            domElement.current.removeChild(space.two.renderer.domElement);
        }
    }

    return <div ref={domElement} />;
}