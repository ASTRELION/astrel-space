/**
 * An object added to the game that must update every frame.
 */
export abstract class GameObject
{
    /**
     * Update the GameObject
     * @param delta the time between the last frame and this one
     */
    abstract update(delta: number): void;
}