import Two from "two.js";
import { Group } from "two.js/src/group";
import { Vector } from "two.js/src/vector";
import { GameObject } from "./gameObject";
import { Space } from "./space";

/**
 * A Body with "physics" applied to it.
 */
export class RigidBody implements GameObject
{
    space: Space;
    two: Two;
    /** Scalar to move by (in pixels) per update. */
    velocity: number;
    /** Maximum velocity. */
    maxVelocity: number;
    /** Normalized direction Vector of this RigidBody. */
    direction: Vector;
    /** Number to decrease velocity by per update. */
    drag: number;
    /** RigidBody mass. */
    mass: number;
    /** This RigidBody's group. */
    group: Group;
    /** True if this RigidBody collides with others, false otherwise. */
    collision: boolean;

    constructor(space: Space)
    {
        this.space = space;
        this.two = space.two;
        this.velocity = 0;
        this.maxVelocity = 5;
        this.direction = new Vector(1, 0);
        this.drag = 0.01;
        this.mass = 1;
        this.collision = false;
        this.group = new Group();
    }

    update(delta: number)
    {
        this.applyVelocity(delta);
        this.applyDrag(delta);
    }

    /**
     * Determine whether this RigidBody Group collides with another.
     * @param other Group to check collision for
     * @returns true if the Groups collide, false otherwise
     */
    collidesWith(other: RigidBody)
    {
        if (!other.collision || !this.collision)
        {
            return false;
        }

        let otherBound = other.group.getBoundingClientRect(false);
        let thisBound = this.group.getBoundingClientRect(false);
        return otherBound.right >= thisBound.left && 
            otherBound.left <= thisBound.right &&
            otherBound.bottom >= thisBound.top && 
            otherBound.top <= thisBound.bottom;
    }

    /**
     * Perform an elastic collision between this RigidBody and the other.
     * @param other the RigidBody to collide with
     */
    collide(other: RigidBody, delta: number)
    {
        this.group.position.add(this.direction.clone().multiply(-1));

        // final velocity of this
        let v1 = 
            (((this.mass - other.mass) / (this.mass + other.mass)) * this.velocity) + 
            (((2 * other.mass) / (this.mass + other.mass)) * other.velocity);
        // final velocity of other
        let v2 =
            (((2 * this.mass) / (this.mass + other.mass)) * this.velocity) +
            (((other.mass - this.mass) / (this.mass + other.mass)) * other.velocity);

        let moveAngle1 = this.directionAngle();
        let moveAngle2 = other.directionAngle();
        let collisionAngle = this.angleTo(other);

        let v1x = (((this.velocity * Math.cos(moveAngle1 - collisionAngle) * (this.mass - other.mass)) + (2 * other.mass * other.velocity * Math.cos(moveAngle2 - collisionAngle))) / (this.mass + other.mass)) * (Math.cos(collisionAngle) + (this.velocity * Math.sin(moveAngle1 - collisionAngle) * Math.cos(collisionAngle + (Math.PI / 2))));
        let v1y = (((this.velocity * Math.cos(moveAngle1 - collisionAngle) * (this.mass - other.mass)) + (2 * other.mass * other.velocity * Math.cos(moveAngle2 - collisionAngle))) / (this.mass + other.mass)) * (Math.sin(collisionAngle) + (this.velocity * Math.sin(moveAngle1 - collisionAngle) * Math.sin(collisionAngle + (Math.PI / 2))));

        // let v2x = (((other.velocity * Math.cos(moveAngle2 - collisionAngle) * (other.mass - this.mass)) + (2 * this.mass * this.velocity * Math.cos(moveAngle1 - collisionAngle))) / (other.mass + this.mass)) * (Math.cos(collisionAngle) + (other.velocity * Math.sin(moveAngle2 - collisionAngle) * Math.cos(collisionAngle + (Math.PI / 2))));
        // let v2y = (((other.velocity * Math.cos(moveAngle2 - collisionAngle) * (other.mass - this.mass)) + (2 * this.mass * this.velocity * Math.cos(moveAngle1 - collisionAngle))) / (other.mass + this.mass)) * (Math.sin(collisionAngle) + (other.velocity * Math.sin(moveAngle2 - collisionAngle) * Math.sin(collisionAngle + (Math.PI / 2))));

        this.velocity = v1;
        this.direction.set(v1x, v1y).normalize();
        
        // other.velocity = v2;
        // other.direction.set(v2x, v2y).normalize();
    }

    /**
     * Get the angle (in radians) from this to another RigidBody
     * @param other another RigidBody
     * @returns angle to other (in radians)
     */
    angleTo(other: RigidBody)
    {
        let thisPos = this.group.position;
        let otherPos = other.group.position;
        return Math.atan((thisPos.y - otherPos.y) / (thisPos.x - otherPos.x));
    }

    directionAngle()
    {
        return Math.atan(this.group.position.y / this.group.position.x);
    }

    rotate(radians: number)
    {
        this.group.rotation += radians;
        this.direction.set(Math.cos(this.group.rotation), Math.sin(this.group.rotation));
    }

    addVelocity(acceleration: number)
    {
        this.velocity = Math.min(this.velocity + acceleration, this.maxVelocity);
    }

    subVelocity(acceleration: number)
    {
        this.velocity = Math.max(this.velocity - acceleration, -this.maxVelocity);
    }

    applyVelocity(delta: number)
    {
        this.group.position.add(this.direction.clone().multiply(this.velocity * delta));
    }

    applyDrag(delta: number)
    {
        // apply drag
        if (Math.abs(this.velocity) > 0)
        {
            this.velocity += (this.velocity > 0 ? -this.drag : this.drag);
        }

        // stop Body at low velocity
        if (Math.abs(this.velocity) <= Math.abs(this.drag))
        {
            this.velocity = 0;
        }
    }

    clone()
    {
        let rb = new RigidBody(this.space);
        rb.velocity = this.velocity;
        rb.maxVelocity = this.velocity;
        rb.direction = this.direction.clone();
        rb.drag = this.drag;
        rb.mass = this.mass;
        rb.group = this.group.clone();
        rb.collision = this.collision;
        return rb;
    }
}