import Two from "two.js";
import { Group } from "two.js/src/group";
import { RoundedRectangle } from "two.js/src/shapes/rounded-rectangle";
import { Text } from "two.js/src/text";
import { RigidBody } from "../rigidbody";
import { Space } from "../space";

export function makeKey(key: string)
{
    let rect = new RoundedRectangle(0, 0, 50, 50, 10);
    rect.fill = "rgba(255, 255, 255, 0.05)";
    rect.stroke = "#dddddd";

    let type = new Text(key.toUpperCase(), 0, 3);
    type.family = "monospace";
    type.fill = "#ffffff";
    type.size = 24;

    return new Group(rect, type);
}

export function makeWASD(space: Space)
{
    let w = makeKey("W");
    w.position.set(55, 0);
    let a = makeKey("A");
    a.position.set(0, 55);
    let s = makeKey("S");
    s.position.set(55, 55);
    let d = makeKey("D");
    d.position.set(110, 55);

    let wasd = new Group(w, a, s, d);

    let body = new RigidBody(space);
    body.mass = 1;
    body.group = wasd;
    body.collision = true;

    wasd.position.set(space.two.width / 2, space.two.height / 1.5);
    space.two.add(wasd);

    return body;
}