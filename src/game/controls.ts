import Two from "two.js";
import { Vector } from "two.js/src/vector";

export class Action
{
    label: string;
    key: string;
    down: boolean;

    constructor(label: string, key: string)
    {
        this.label = label;
        this.key = key;
        this.down = false;
    }
}

export let actions = {
    UP: new Action("UP", "w"),
    DOWN: new Action("DOWN", "s"),
    RIGHT: new Action("RIGHT", "d"),
    LEFT: new Action("LEFT", "a"),
    SHOOT: new Action("SHOOT", " "),
};

export let mousePosition = new Vector(0, 0);

function findActionByKey(key: string): Action | undefined
{
    return Object.values(actions).find((x) => x.key == key.toLowerCase());
}

/**
 * Adds relevant event listeners.
 * @param element 
 */
export function init(element: HTMLElement)
{
    window.addEventListener("keydown", (e: KeyboardEvent) =>
    {
        let k = e.key.toLowerCase();
        let action = null;
        if (!(action = findActionByKey(k))) return;
        action.down = true;
    });
    
    window.addEventListener("keyup", (e: KeyboardEvent) =>
    {
        let k = e.key.toLowerCase();
        let action = null;
        if (!(action = findActionByKey(k))) return;
        action.down = false;
    });

    window.addEventListener("mousemove", (e: MouseEvent) =>
    {
        mousePosition.set(e.x, e.y)
    });
}