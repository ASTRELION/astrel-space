import type { GatsbyConfig } from "gatsby";

const config: GatsbyConfig = {
    siteMetadata: {
        title: `AstrelSpace`,
        siteUrl: `https://astrelion.com`,
    },
    // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
    // If you use VSCode you can also use the GraphQL plugin
    // Learn more at: https://gatsby.dev/graphql-typegen
    graphqlTypegen: true,
    // required for GitLab pages
    // https://www.gatsbyjs.com/docs/deploying-to-gitlab-pages/
    pathPrefix: "/astrel-space",
    plugins: [
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                icon: "src/images/icon.png",
            },
        },
    ],
};

export default config;
